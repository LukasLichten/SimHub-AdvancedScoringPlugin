﻿using Newtonsoft.Json;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    /// <summary>
    /// Wrapper for a Delta type, manages saving and loading the Data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ManagedDelta<T> where T : IComputeMix<T>
    {
        public const string PLUGINS_DATA_FOLDER = "PluginsData";
        public const string DATA_FOLDER = "AdvancedScoring";

        public StorageType Store { get; set; }
        public Delta<T> Reference { get; private set; }
        public DataKey CurrentConfig { get; private set;}
        public string Name { get; private set; }
        public Delta<T> Cache { get; private set; }

        public Action<DataKey> UpdatedReference { get; set; }
        
        internal Dictionary<DataKey, IEval> Configs { get; private set; }
        private PluginManager PluginManager { get; set; }

        public ManagedDelta(PluginManager pluginManager, string name, StorageType storageType)
        {
            PluginManager = pluginManager;
            Name = name;
            Store = storageType;

            Cache = new Delta<T>();

            Task.Factory.StartNew(LoadData); //Async loading of Deltas, makes the plugin load rapidly, but deltas take a moment to load in
            //LoadData();
        }

        public void WriteData()
        {
            if (Configs == null)
                return; //can happen when switching games too quickly

            if (CurrentConfig != null && Reference != null)
            {
                if (Configs.ContainsKey(CurrentConfig))
                    Configs[CurrentConfig] = Reference;
                else
                    Configs.Add(CurrentConfig, Reference);
            }

            string dataFolder = GetDataFolderPath(PluginManager);
            if (!Directory.Exists(dataFolder))
                Directory.CreateDirectory(dataFolder);

            if (Store == StorageType.Seperate)
            {
                if (!Directory.Exists(Path.Combine(GetDataFolderPath(PluginManager), Name)))
                    Directory.CreateDirectory(Path.Combine(GetDataFolderPath(PluginManager), Name));
            }


            LinkedList<DataWrapper<T>> list = new LinkedList<DataWrapper<T>>();

            foreach (var item in Configs)
            {
                DataWrapper<T> wrapper = new DataWrapper<T>(item.Key, item.Value);

                if (item.Value is Delta<T> delta)
                {
                    delta.Optimize();

                    if (Store == StorageType.Seperate)
                    {
                        wrapper.Delta = DeltaPlaceholder.GenerateFromDelta<T>(delta);

                        //Writing the Delta to seperate file
                        WriteSingleDeltaToFile(Path.Combine(GetDataFolderPath(PluginManager), Name, $"{item.Key.TrackName}_{item.Key.CarName}.dat"), new DataWrapper<T>(item.Key, delta), StorageType.Binary);
                    }
                }

                if (item.Value != null)
                {
                    list.AddFirst(wrapper);
                }
            }
            
            DataWrapper<T>[] arr = list.ToArray();

            if (Store == StorageType.Off)
                return;

            if (Store == StorageType.Json || Store == StorageType.Seperate)
            {
                string serialized = JsonConvert.SerializeObject(arr, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });

                

                WriteJSONToFile(Path.Combine(dataFolder, Name + ".json"), serialized);
            }
            else if (Store == StorageType.Binary)
            {
                WriteBinaryToFile<DataWrapper<T>[]>(Path.Combine(dataFolder, Name + ".dat"), arr);
            }
            
        }

        public void LoadData()
        {
            Configs = null; //Indicates data is not loaded
            Dictionary<DataKey, IEval> tempConfigs = new Dictionary<DataKey, IEval>(); //This temp is necessary

            if (Store == StorageType.Off)
            {
                Configs = tempConfigs; //We have to indicate loading is done
                return;
            }
                

            DataWrapper<T>[] data = null;

            if (Store == StorageType.Json || Store == StorageType.Seperate)
            {
                string serialized = ReadJSONFromFile(Path.Combine(GetDataFolderPath(PluginManager), Name + ".json"));

                if (serialized == null)
                {
                    Configs = tempConfigs; //We have to indicate loading is done
                    return;
                }

                try
                {
                    data = JsonConvert.DeserializeObject<DataWrapper<T>[]>(serialized, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });
                }
                catch
                {
                    //Potential for malformed json, which in deserialization could throw an exception
                }
            }
            else if (Store == StorageType.Binary)
            {
                data = ReadBinaryFromFile<DataWrapper<T>[]>(Path.Combine(GetDataFolderPath(PluginManager), Name + ".dat"));
            }

            

            if (data != null && data.Length != 0)
            {
                foreach (var item in data)
                {
                    if (item.Delta is Delta<T> delta)
                        delta.Optimize();

                    tempConfigs.Add(item.DataKey, item.Delta);
                }
            }

            //System.Threading.Thread.Sleep(30000);

            Configs = tempConfigs;

            //So the UI shows all deltas up, as the UI will likely load before the deltas have
            if (UpdatedReference != null)
                UpdatedReference.BeginInvoke(null, null, null);

        }

        public Delta<T> GetDelta(DataKey key)
        {
            if (Configs == null)
                return null;

            if (Configs.ContainsKey(key))
            {
                IEval item = Configs[key];

                if (item is DeltaPlaceholder)
                {
                    var dataWrapper = ReadSingleDeltaFromFile(Path.Combine(GetDataFolderPath(PluginManager), Name, $"{key.TrackName}_{key.CarName}.dat"), StorageType.Binary);

                    if (dataWrapper == null || !dataWrapper.DataKey.Equals(key))
                    {
                        DeleteDelta(key); // We remove deltas that can't be loaded
                        UpdatedReference?.BeginInvoke(null, null, null);
                        return null;
                    }
                    

                    if (dataWrapper.Delta is Delta<T> delta)
                    {
                        delta.Optimize();
                        Configs[key] = delta; //Caching the Delta for future use
                        return delta;
                    }

                }
                else if (item is Delta<T> del)
                {
                    return del;
                }
            }

            return null;
        }

        public void DeleteDelta(DataKey key)
        {
            if (!Configs.ContainsKey(key))
                return;

            Configs.Remove(key);

            if (Store == StorageType.Seperate)
            {
                //We have to delete the file that stores the Delta
                string path = Path.Combine(GetDataFolderPath(PluginManager), Name, $"{key.TrackName}_{key.CarName}.dat");

                if (File.Exists(path))
                    File.Delete(path);
            }
        }

        public virtual void Update(T Data, double lapFrac)
        {
            //Updating the Cache
            Cache.Add(lapFrac, Data);

            
            //Making sure the config has not changed
            DataKey test = new DataKey(PluginManager);
            if (!test.Equals(CurrentConfig) && Configs != null)
            {
                if (CurrentConfig != null) //Avoids saving immediatly upon loading
                    WriteData(); //also saves the current reference into the Configs

                if (Configs.ContainsKey(test))
                {
                    //Loads this data
                    Reference = GetDelta(test);
                }
                else
                {
                    Reference = null;
                }

                CurrentConfig = test;
                Cache = new Delta<T>();

                if (UpdatedReference != null)
                    UpdatedReference.BeginInvoke(CurrentConfig, null, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flush">wether the cache should be written to the reference value</param>
        public void NewLap(bool flush)
        {
            Cache.GenerateStatistics();

            if (flush)
            {
                Reference = Cache;

                if (UpdatedReference != null)
                    UpdatedReference.BeginInvoke(CurrentConfig, null, null);
            }

            if (Reference != null)
                Cache = new Delta<T>(Reference.Length);
            else
                Cache = new Delta<T>();
        }

        public void ExportSingleDelta(DataKey key, string location, StorageType storageType)
        {
            if (key.Equals(CurrentConfig))
            {
                //Currently loaded config, so saving it
                WriteData();
            }

            
            if (Configs.ContainsKey(key))
                WriteSingleDeltaToFile(location, new DataWrapper<T>(key, GetDelta(key)), storageType);
            else
                return;

            
        }

        public void WriteSingleDeltaToFile(string location, DataWrapper<T> dataWrapper, StorageType storageType)
        {
            DataWrapper<T>[] arr = new DataWrapper<T>[] { dataWrapper };

            if (Directory.Exists(Path.GetDirectoryName(location)))
            {
                if (storageType == StorageType.Json)
                {
                    if (Path.GetExtension(location) != ".json")
                    {
                        location = Path.Combine(Path.GetDirectoryName(location), Path.GetFileNameWithoutExtension(location), ".json");
                    }

                    string json = JsonConvert.SerializeObject(arr, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });

                    WriteJSONToFile(location, json);
                }
                else if (storageType == StorageType.Binary)
                {
                    WriteBinaryToFile<DataWrapper<T>[]>(location, arr);
                }
            }
        }

        public DataWrapper<T> ReadSingleDeltaFromFile(string location, StorageType storageType)
        {
            if (!File.Exists(location))
                return null;

            DataWrapper<T>[] data = null;

            if (storageType == StorageType.Json)
            {
                string serialized = ReadJSONFromFile(location);

                if (serialized == null)
                {
                    return null;
                }

                try
                {
                    data = JsonConvert.DeserializeObject<DataWrapper<T>[]>(serialized, new JsonSerializerSettings() { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto });
                }
                catch
                {
                    //Potential for malformed json, which in deserialization could throw an exception
                }
            }
            else if (storageType == StorageType.Binary)
            {
                data = ReadBinaryFromFile<DataWrapper<T>[]>(location);
            }

            if (data == null || data.Length == 0)
                return null;

            return data[0];
        }

        public void ImportDelta(DataWrapper<T> data)
        {
            Delta<T> delta = data.Delta as Delta<T>;

            if (delta == null)
                return;

            delta.Optimize();
            if (delta.Length == 0)
                return; //Defective delta

            if (data.DataKey.Equals(CurrentConfig))
            {
                Reference = delta;
            }
            else if (Configs.ContainsKey(data.DataKey))
            {
                Configs[data.DataKey] = delta;
            }
            else
            {
                Configs.Add(data.DataKey, delta);
            }

            WriteData();
        }

        public static string GetDataFolderPath(PluginManager pluginManager)
        {
            return Path.Combine(PLUGINS_DATA_FOLDER, pluginManager.GameName, DATA_FOLDER);
        }

        public static void WriteJSONToFile(string path, string dataToWrite)
        {
            StreamWriter writer = null;

            try
            {
                writer = new StreamWriter(path, false, Encoding.UTF8);
                writer.Write(dataToWrite);
                writer.Flush();
            }
            catch (IOException ex)
            {
                SimHub.Logging.Current.Error("AdvancedScoring: Exception thrown when trying save Delta Data: " + ex.Message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public static string ReadJSONFromFile(string path)
        {
            StreamReader reader = null;
            string output = null;

            if (!File.Exists(path))
            {
                SimHub.Logging.Current.Info("AdvancedScoring: Delta Data does not exist, no data will be loaded");
                return output;
            }

            try
            {
                reader = new StreamReader(path, Encoding.UTF8);
                output = reader.ReadToEnd();
            }
            catch (IOException ex)
            {
                SimHub.Logging.Current.Error("AdvancedScoring: Exception thrown when trying read Delta Data: " + ex.Message);
                output = null;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return output;
        }

        public static void WriteBinaryToFile<O>(string path, O objectToWrite)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = null;

            try
            {
                stream = new FileStream(path, FileMode.Create);
                formatter.Serialize(stream, objectToWrite);

                stream.Flush();
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("AdvancedScoring: Exception thrown when trying save Delta Data: " + ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public static O ReadBinaryFromFile<O>(string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = null;

            O output = default(O);

            if (!File.Exists(path))
            {
                SimHub.Logging.Current.Info("AdvancedScoring: Delta Data does not exist, no data will be loaded");
                return output;
            }

            try
            {
                stream = new FileStream(path, FileMode.Open);
                object data = formatter.Deserialize(stream);

                if (data is O)
                    output = (O)data;
            }
            catch (Exception ex)
            {
                SimHub.Logging.Current.Error("AdvancedScoring: Exception thrown when trying load Delta Data: " + ex.Message);
                output = default(O);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return output;
        }

        [Serializable]
        public class DataKey
        {
            public string CarName { get; set; }
            public string TrackName { get; set; }

            public DataKey() { }

            public DataKey(PluginManager pluginManager)
            {
                var data = pluginManager.LastData.NewData;

                CarName = (data.CarClass != null && data.CarClass != "" ? data.CarClass + "-" : "") + data.CarModel;
                TrackName = data.TrackCode;
            }

            public override int GetHashCode()
            {
                return CarName.GetHashCode() + TrackName.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (!(obj is DataKey))
                    return false;

                DataKey tag = (DataKey)obj;

                return CarName.Equals(tag.CarName) && TrackName.Equals(tag.TrackName);
            }
        }

        [Serializable]
        public class DataWrapper<N> where N : IComputeMix<N>
        {
            public DataKey DataKey { get; set; }
            public IEval Delta { get; set; }

            public DataWrapper() { }

            public DataWrapper(DataKey key, IEval delta)
            {
                DataKey = key;
                Delta = delta;
            }
        }

        [Serializable]
        public class DeltaPlaceholder : IEval
        {
            public object Eval { get; set; }

            public DeltaPlaceholder() { }

            public static DeltaPlaceholder GenerateFromDelta<O>(Delta<O> delta) where O : IComputeMix<O>
            {
                return new DeltaPlaceholder()
                {
                    Eval = delta.Eval,
                };
            }
        }
    }
}
