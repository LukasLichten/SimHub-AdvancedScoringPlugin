﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    /// <summary>
    /// A helper class to deal with keeping the values
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LinkedQueue<T>
    {
        private readonly int limit;

        private Node<T> start;

        public int Length { get; private set; }

        public LinkedQueue(int limit)
        {
            this.limit = limit;
        }

        public void Clear()
        {
            start = null;
            Length = 0;
        }

        public void Add(T value)
        {
            Node<T> newStart = new Node<T>();

            newStart.next = start;
            newStart.value = value;

            start = newStart;
            Length++;

            if (Length > limit)
            {
                Node<T> cutPoint = start;

                for (int i = 0; i < (limit - 1); i++)
                    cutPoint = cutPoint.next;


                cutPoint.next = null;
                Length = limit;
            }
        }

        public T GetValue(int index)
        {
            if (index < 0 || index >= Length)
            {
                throw new ArgumentException("Index out of bounds!");
            }

            Node<T> node = start;
            for (int i = 0; i < index; i++)
                node = node.next;

            if (node != null)
                return node.value;
            else
                return default(T);
        }

        public T[] GetValues()
        {
            T[] arr = GetValuesOnLength();

            if (Length < limit)
            {
                T[] arr2 = new T[limit];

                for (int i = 0; i < arr.Length; i++)
                    arr2[i] = arr[i];

                arr = arr2;

                for (int i = Length; i < limit; i++)
                    arr[i] = default(T);
            }

            return arr;
        }

        public T[] GetValuesOnLength()
        {
            T[] arr = new T[Length];

            Node<T> node = start;
            for (int i = 0; i < Length; i++)
            {
                arr[i] = node.value;

                node = node.next;
            }

            return arr;
        }


        private class Node<M>
        {
            public Node<M> next;

            public M value;
        }
    }
}
