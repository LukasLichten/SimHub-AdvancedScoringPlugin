﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    [Serializable]
    public class DoubleWrapper : IComputeMix<DoubleWrapper>
    {

        public readonly double Value;

        public DoubleWrapper(double value)
        {
            Value = value;
        }

        public DoubleWrapper ProduceMixture(DoubleWrapper other, double sliderToOther)
        {
            if (sliderToOther <= 0.0 || other == null)
                return this;
            if (sliderToOther >= 1.0)
                return other;
            
            double delta = other.Value - Value;

            double comp = (delta * sliderToOther) + Value;
            
            return new DoubleWrapper(comp);
        }

        public DoubleWrapper Minimum(DoubleWrapper other)
        {
            if (Value < other.Value || other == null)
                return this;
            else
                return other;
        }

        public DoubleWrapper Maximum(DoubleWrapper other)
        {
            if (Value > other.Value || other == null)
                return this;
            else
                return other;
        }

        public override string ToString()
        {
            return "" + Value;
        }

        public object GetValue()
        {
            return Value;
        }

        public DoubleWrapper Subtract(DoubleWrapper subtrahend)
        {
            double result = Value;
            if (subtrahend != null)
                result -= subtrahend.Value;

            return new DoubleWrapper(result);
        }
    }
}
