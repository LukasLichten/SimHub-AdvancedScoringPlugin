﻿using MathNet.Numerics.Interpolation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    [Serializable]
    public class Delta<T> : IEval where T: IComputeMix<T>
    {
        public const int DEFAULT_SIZE = 60 * 60 * 3; //Default array length, 3min at 60fps
        private const int EXTRA_SIZE_TO_ADD = 60 * 60; //1min, added extra when defining it with a certain length
        private const int SIZE_INCREMENT_FACTOR = 2; //By what factor the Array gets lengthend if it runs out of space
        private const double IGNORE_LIMIT = 0.2; //If the new addition is this value smaller or bigger then the last it is ignored
        private const int GUESS_STEP_THREASHOLD = 10; //For the Get function the number of estimated steps before we just guess and jump rather then iterating
        private const int MAX_GUESSES = 10; //Number of guesses prior to going iterative to prevent deadlock

        public Tupple<T>[] Data { get; set; } //This has to be Public to be included when written into JSON, do not use this

        public int Length { get; set; }

        public T MinValue { get; set; }
        public T MaxValue { get; set; }
        public T AverageValue { get; set; }

        public object Eval { get; set; }

        [NonSerialized()]
        private LinearSpline OptimizedInterporlator;

        public Delta() : this (DEFAULT_SIZE - EXTRA_SIZE_TO_ADD) { }

        public Delta(int previousSize)
        {
            Data = new Tupple<T>[previousSize + EXTRA_SIZE_TO_ADD];
            Length = 0;
        }

        public void Add(double lapFraction, T value)
        {
            Tupple<T> tupple = new Tupple<T>(lapFraction, value);

            if (Data.Length <= Length) //Size Check
            {
                //Size Check Failed, having to remake the Array
                Tupple<T>[] oldData = Data;

                Data = new Tupple<T>[oldData.Length * SIZE_INCREMENT_FACTOR];

                for (int i = 0; i < oldData.Length; i++)
                {
                    Data[i] = oldData[i];
                }
            }

            double previousValue = 0;
            if (Length > 0)
                previousValue = Data[Length - 1].LapFraction;

            if (tupple.LapFraction > (previousValue + IGNORE_LIMIT))
            {
                //For some reason there was a missread from where the data came, for example another part of the track
                //Or this is a left over part from a previous lap
                //Data is ignored
                return;
            }

            if (Length > 0 && tupple.LapFraction <= previousValue) //Length has to be over 0 as there would be no point to this if it was 0
            {
                //For some reason we have a lap frac that is smaller then the previous, can be the driver going the wrong way
                //We delete all values that are further up then this to keep the incremental state of the list up

                if (tupple.LapFraction < (previousValue - IGNORE_LIMIT))
                {
                    //Except the threashold is stepped over. We could potentially get data for the next lap, so the lap frac would be 0, which would delete everything, then the value is ignored
                    return;
                }

                while (Length > 0 && tupple.LapFraction <= Data[Length - 1].LapFraction)
                {
                    Length--;
                }
            }

            Data[Length] = tupple;
            Length++;
        }

        public void GenerateStatistics()
        {
            Optimize();
            if (Length == 0)
                return;

            MinValue = Data[0].Value;
            MaxValue = Data[0].Value;
            AverageValue = Data[0].Value;

            for (int i = 1; i < Length; i++)
            {
                Tupple<T> value = Data[i];

                MinValue = MinValue.Minimum(value.Value);
                MaxValue = MaxValue.Maximum(value.Value);

                AverageValue = AverageValue.ProduceMixture(value.Value, 1.0 / (i + 1.0)); //Evaluates every value, pretty slow
            }
        }

        public T GetValue(double lapFrac)
        {
            if (Length == 0)
                return default;

            if (OptimizedInterporlator != null)
            {
                object val = new DoubleWrapper(OptimizedInterporlator.Interpolate(lapFrac));
                return (T)val;
            }

            double minLapFrac = Data[0].LapFraction;
            double maxLapFrac = Data[Length - 1].LapFraction;

            //Special out of bound cases
            if (lapFrac <= minLapFrac || lapFrac >= maxLapFrac)
            {
                return lapFrac >= maxLapFrac ? Data[Length - 1].Value : Data[0].Value;
            }

            //Finding the data pair to compare to
            double stepLapFrac = (maxLapFrac - minLapFrac) / Length;

            int estimate = (int)((lapFrac - minLapFrac) / stepLapFrac);
            if (estimate + 1 >= Length)
                estimate--;

            int guesses = 0;

            while (!(Data[estimate].LapFraction <= lapFrac && lapFrac <= Data[estimate + 1].LapFraction))
            {
                double delta = lapFrac - Data[estimate].LapFraction;
                if (delta > 0)
                    delta = lapFrac - Data[estimate + 1].LapFraction;

                if (guesses != MAX_GUESSES && (Math.Abs(delta) / stepLapFrac) > GUESS_STEP_THREASHOLD)
                {
                    //We take another estimation trying to get closer
                    estimate += (int)(delta / stepLapFrac);
                    guesses++;
                }
                else
                {
                    //We are so close we just iterate and try again
                    if (delta > 0)
                        estimate++;
                    else
                        estimate--;
                }

                if (estimate + 1 >= Length)
                    estimate = Length - 2;
                else if (estimate < 0)
                    estimate = 0;
            }

            //Taking the data pair and calculating the middle
            Tupple<T> Lower = Data[estimate];
            Tupple<T> Upper = Data[estimate + 1];

            double point = (lapFrac - Lower.LapFraction) / (Upper.LapFraction - Lower.LapFraction);
            return Lower.Value.ProduceMixture(Upper.Value, point);
        }

        public void Optimize()
        {
            if (Data == null || Data.Length < Length || (Length > 0 && Data[0] == null) || Length == 0)
            {
                Data = new Tupple<T>[1];
                Length = 0;
                return;
            }
            

            if (Length > 1 && Data[0].Value is DoubleWrapper)
            {
                // We can optimize call backs by using an interperlator
                double[] lapfrac = new double[Length];
                double[] values = new double[Length];

                Tupple<T>[] opt = new Tupple<T>[Length];

                for (int i = 0; i < Length; i++)
                {
                    opt[i] = Data[i];
                    lapfrac[i] = Data[i].LapFraction;
                    if (Data[i].Value is DoubleWrapper val)
                    {
                        values[i] = val.Value;
                    }
                }

                OptimizedInterporlator = LinearSpline.InterpolateSorted(lapfrac, values);
            }
            else if (Data.Length != Length)
            {
                Tupple<T>[] opt = new Tupple<T>[Length];

                for (int i = 0; i < opt.Length; i++)
                {
                    opt[i] = Data[i];
                }

                Data = opt;

            }
        }

        public bool IsValidLap(double lapFracBegin, double lapFracEnd)
        {
            if (Data == null || Data.Length < Length || (Length > 0 && Data[0] == null))
            {
                Optimize();
                return false;
            }

            if (Length == 0)
                return false;

            return Data[0].LapFraction < lapFracBegin && Data[Length - 1].LapFraction > lapFracEnd;
        }

        public bool IsValidLap()
        {
            return this.IsValidLap(0.1, 0.9);
        }

        [Serializable]
        public class Tupple<N>
        {
            public double LapFraction;
            public N Value;

            public Tupple() { }
            public Tupple(double lapFraction, N value)
            {
                LapFraction = lapFraction;
                Value = value;
            }
        }
    }
}
