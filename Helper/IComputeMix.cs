﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    public interface IComputeMix<T>
    {
        /// <summary>
        /// Creates a mixture of these two values
        /// </summary>
        /// <param name="other">The other value</param>
        /// <param name="sliderToOther">this is a value between 0.0 and 1.0, with 0 being this, and 1 being other, and everything between a blend of the two values</param>
        /// <returns></returns>
        T ProduceMixture(T other, double sliderToOther);

        T Minimum(T other);

        T Maximum(T other);

        object GetValue();

        //double GetDoubleValue();

        T Subtract(T subtrahend);
    }
}
