﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    public class PitlaneDelta
    {
        private AttachedProperty<TimeSpan> TimeInPits { get; set; }
        private AttachedProperty<TimeSpan> DeltaToTarget { get; set; }
        private AttachedProperty<bool> DeltaAvailable { get; set; }

        private ManagedDelta<DoubleWrapper> ManagedDelta { get; set; }

        private AsSettings Settings { get; set; }

        //We only store deltas recorded from DriveThroughs
        private bool IsJustDriveThrough { get; set; }
        private bool InPitlane { get; set; }
        private Stopwatch Timer { get; set; }
        private int Lap { get; set; }

        public PitlaneDelta(AdvancedScoring advancedScoring)
        {
            ManagedDelta = new ManagedDelta<DoubleWrapper>(advancedScoring.PluginManager, "PitlaneDelta", StorageType.Seperate);
            Settings = advancedScoring.Settings;

            Timer = new Stopwatch();
            TimeInPits = new AttachedProperty<TimeSpan> { Value = TimeSpan.Zero };
            DeltaToTarget = new AttachedProperty<TimeSpan> { Value = TimeSpan.Zero };
            DeltaAvailable= new AttachedProperty<bool> { Value = false };

            advancedScoring.PluginManager.AttachProperty("PitlaneTime", advancedScoring.GetType(), TimeInPits);
            advancedScoring.PluginManager.AttachProperty("PitlaneDeltaToTarget", advancedScoring.GetType(), DeltaToTarget);
            advancedScoring.PluginManager.AttachProperty("PitlaneDeltaAvailable", advancedScoring.GetType(), DeltaAvailable);
        }

        public void Update(PluginManager pluginManager, GameData data)
        {
            double lapFrac = (data.NewData.CompletedLaps + data.NewData.TrackPositionPercent) - Lap;
            

            if (data.NewData.IsInPitLane == 1 && !InPitlane)
            {
                //Just entered pitlane
                Timer.Restart();
                Lap = data.NewData.CompletedLaps;
                ManagedDelta.NewLap(false);

                lapFrac = (data.NewData.CompletedLaps + data.NewData.TrackPositionPercent) - Lap; // need to recalculate LapFrac, due to changing Lap
                DoubleWrapper wrapper = new DoubleWrapper(0.0);

                ManagedDelta.Update(wrapper, lapFrac); //We run this function to update the current config
                //Then we forcibly add the first value, important to deal with the maximum diviation
                ManagedDelta.Cache.Data[0] = new Delta<DoubleWrapper>.Tupple<DoubleWrapper>(lapFrac, wrapper);
                ManagedDelta.Cache.Length = 1;

                IsJustDriveThrough = true;
                InPitlane = true;
                DeltaAvailable.Value = ManagedDelta.Reference != null;
            }
            else if (data.NewData.IsInPitLane != 1 && InPitlane)
            {
                //Just exited pitlane
                Timer.Stop();

                //We assume a worst case scenario
                //So we assume the pitlane for recording and TargetTime is the previous update
                //For Pitlane Time it is from the timer
                if (ManagedDelta.Cache?.Length > 0)
                {
                    var tupple = ManagedDelta.Cache.Data[ManagedDelta.Cache.Length - 1];
                    double time = tupple.Value.Value;

                    WriteDeltaToTarget(lapFrac, time);

                    ManagedDelta.Cache.Eval = time;
                    if (IsJustDriveThrough && data.NewData.CompletedLaps >= Lap && ManagedDelta.Cache.Length > 1)
                        ManagedDelta.NewLap(true);
                }
                TimeInPits.Value = TimeSpan.FromMilliseconds(Timer.Elapsed.TotalMilliseconds);

                InPitlane = false;
                DeltaAvailable.Value = ManagedDelta.Reference != null;
            }
            else if (data.NewData.IsInPitLane == 1 && InPitlane)
            {
                //In pits
                double time = Timer.Elapsed.TotalSeconds;

                DoubleWrapper wrapper = new DoubleWrapper(time);
                ManagedDelta.Update(wrapper, lapFrac);

                if (data.NewData.SpeedKmh < 10)
                    IsJustDriveThrough = false;

                WriteDeltaToTarget(lapFrac, time);
                TimeInPits.Value = TimeSpan.FromMilliseconds(Timer.Elapsed.TotalMilliseconds);
            }
        }

        public void End()
        {
            ManagedDelta.WriteData();
        }

        public void Wipe()
        {
            ManagedDelta.NewLap(false);
            
            if (Timer.IsRunning)
                Timer.Restart();
            else
                Timer.Reset();

            IsJustDriveThrough = false;
        }

        private void WriteDeltaToTarget(double lapFrac, double time)
        {
            double delta = 0;

            if (ManagedDelta.Reference != null)
            {
                double baseline = ManagedDelta.Reference.GetValue(lapFrac).Value;
                double total = (double)ManagedDelta.Reference.Eval;

                double estimatedTimeToExit = total - baseline;

                delta = Settings.PitlaneTargetTime - (estimatedTimeToExit + time);
            }

            DeltaToTarget.Value = TimeSpan.FromSeconds(delta);
        }
    }
}
