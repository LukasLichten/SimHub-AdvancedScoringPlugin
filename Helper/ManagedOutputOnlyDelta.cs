﻿using SimHub.Plugins;
using System;

namespace Advanced_Scoring.Helper
{
    public class ManagedOutputOnlyDelta<T, D> : ManagedOutputDelta<T, D> where T : IComputeMix<T>
    {
        public ManagedOutputOnlyDelta(PluginManager pluginManager, string name, StorageType storageType, Type pluginType) : base(pluginManager, name, storageType, pluginType) { }

        public override void Update(T Data, double lapFrac)
        {
            OutputDelta(Data, lapFrac);
        }
    }
}
