﻿using Advanced_Scoring.Helper;
using Advanced_Scoring.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace Advanced_Scoring.UI.Controller
{
    public class DeltaConfigController
    {
        private ManagedDelta<DoubleWrapper> ManagedDelta { get; set; }
        private Dispatcher ModelDispatcher { get; set; }

        public DeltaModel DataModel { get; private set; }
        public bool ImportMode { get; set; }

        public DeltaConfigController(ManagedDelta<DoubleWrapper> managedDelta)
        {
            ModelDispatcher = Dispatcher.CurrentDispatcher;

            ManagedDelta = managedDelta;
            DataModel = new DeltaModel();

            ManagedDelta.UpdatedReference += UpdatedReference;

            LoadDeltas();
        }

        public void LoadDeltas()
        {
            DataModel.Deltas.Clear();
            if (ManagedDelta.Configs != null)
            {
                foreach (var item in ManagedDelta.Configs)
                {
                    TimeSpan span = TimeSpan.Zero;
                    if (item.Value.Eval is double @double)
                        span = TimeSpan.FromSeconds(@double);

                    DeltaModel.DataRow row = new DeltaModel.DataRow(item.Key.CarName, item.Key.TrackName, span);
                    DataModel.Deltas.Add(row);
                }
            }
        }

        public void UpdatedReference(ManagedDelta<DoubleWrapper>.DataKey key)
        {
            if (DataModel.Deltas.Count == 0 || (DataModel.Deltas.Count != ManagedDelta.Configs.Count && key == null))
            {
                //Due to async loading the list of deltas won't be there when generating, this function is called when they are done
                //Also to allow deleting items from the backends side
                ModelDispatcher.Invoke(LoadDeltas);
            }

            if (ManagedDelta.Reference == null || key == null)
                return;
            DeltaModel.DataRow row = GetDataRow(key);

            object eval = ManagedDelta.Reference.Eval;

            if (eval is double @double)
                row.SetLapTime(TimeSpan.FromSeconds(@double));
            else
                row.SetLapTime(TimeSpan.Zero);

        }

        private DeltaModel.DataRow GetDataRow(ManagedDelta<DoubleWrapper>.DataKey key)
        {
            DeltaModel.DataRow row = null;

            foreach (var item in DataModel.Deltas)
            {
                if (item.CarName == key.CarName && item.TrackName == key.TrackName)
                {
                    row = item;
                    break;
                }
            }

            if (row == null)
            {
                row = new DeltaModel.DataRow(key.CarName, key.TrackName, TimeSpan.Zero);
            }
            else
            {
                ModelDispatcher.Invoke(() => DataModel.Deltas.Remove(row));
            }

            ModelDispatcher.Invoke(() => DataModel.Deltas.Insert(0, row));
            return row;
        }

        public void DeleteItem(DeltaModel.DataRow data)
        {
            var key = new ManagedDelta<DoubleWrapper>.DataKey
            {
                CarName = data.CarName,
                TrackName = data.TrackName
            };

            if (key.Equals(ManagedDelta.CurrentConfig))
            {
                //ManagedDelta.Reference = null; //needs to clear the current config too
            }

            ManagedDelta.DeleteDelta(key);

            

            //Update UI
            DataModel.Deltas.Remove(data);
        }

        public void StartCopyItem(DeltaModel.DataRow data)
        {
            
        }

        public void CopyToItem(DeltaModel.DataRow data, string trackName, string carName)
        {

        }

        public void ExportItem(DeltaModel.DataRow data)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "Json File (*.json)|*.json";
            dialog.FileName = dialog.InitialDirectory + data.TrackName + "_" + data.CarName + "_" + data.LapTime.Replace(":",".") + ".json";
            dialog.Title = "Save Target Json";

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var key = new ManagedDelta<DoubleWrapper>.DataKey();
                key.CarName = data.CarName;
                key.TrackName = data.TrackName;

                ManagedDelta.ExportSingleDelta(key, dialog.FileName, StorageType.Json);
            }
        }

        public void ImportItem()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Json File (*.json)|*.json",
                Title = "Load Target Json"
            };

            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                ManagedDelta<DoubleWrapper>.DataWrapper<DoubleWrapper> wrapper = ManagedDelta.ReadSingleDeltaFromFile(dialog.FileName, StorageType.Json);

                if (wrapper == null || wrapper.DataKey == null || wrapper.Delta == null || !(wrapper.Delta is Delta<DoubleWrapper>))
                {
                    MessageBox.Show("Failed to load Delta out of file. Make sure it is the correct file and that the data is undamaged", "That's not right!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                TimeSpan span = TimeSpan.Zero;
                if (wrapper.Delta.Eval is double newTime)
                    span = TimeSpan.FromSeconds(newTime);

                

                //Overwrite screenario
                if ((wrapper.DataKey.Equals(ManagedDelta.CurrentConfig) && ManagedDelta.Reference != null) || ManagedDelta.Configs.ContainsKey(wrapper.DataKey))
                {
                    object eval;
                    if (wrapper.DataKey.Equals(ManagedDelta.CurrentConfig))
                        eval = ManagedDelta.Reference.Eval;
                    else
                        eval = ManagedDelta.Configs[wrapper.DataKey].Eval;

                    TimeSpan oldSpan = TimeSpan.Zero;
                    if (eval is double oldTime)
                        oldSpan = TimeSpan.FromSeconds(oldTime);

                    string old = oldSpan.ToString(@"mm\:ss\.fff");
                    string imp = span.ToString(@"mm\:ss\.fff");


                    var result2 = MessageBox.Show("The Config " + wrapper.DataKey.CarName + " at " + wrapper.DataKey.TrackName + " already exists.\nDo you want to overwrite the " + old + " with a " + imp + "?", "Are you sure?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                    if (result2 == DialogResult.Cancel)
                        return; //User exits


                }

                if (wrapper.Delta is Delta<DoubleWrapper> del && del.Length == 0)
                {
                    MessageBox.Show("Unable to add Delta, data seems damaged","Failure!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                ManagedDelta.ImportDelta(wrapper);

                GetDataRow(wrapper.DataKey).SetLapTime(span);
            }
        }
    }
}
