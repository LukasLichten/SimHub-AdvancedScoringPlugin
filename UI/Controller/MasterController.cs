﻿using Advanced_Scoring.UI.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.UI.Controller
{
    public class MasterController
    {
        public MainControl MainView { get; private set; }
        public SettingsController SettingsController { get; private set; }
        public DeltaConfigController PersonalBestController { get; private set; }
        public DeltaConfigController TargetDeltaController { get; private set; }

        public MasterController(AdvancedScoring advancedScoring)
        {
            MainView = new MainControl();

            SettingsController = new SettingsController(advancedScoring);
            PersonalBestController = new DeltaConfigController(advancedScoring.DeltaManager.OverallBestLap);
            TargetDeltaController = new DeltaConfigController(advancedScoring.DeltaManager.TargetTimes);
            TargetDeltaController.ImportMode = true;

            MainView.SettingsView.SetController(SettingsController);
            MainView.PersonalDeltaView.SetController(PersonalBestController);
            MainView.TargetDeltaView.SetController(TargetDeltaController);
        }
    }
}
