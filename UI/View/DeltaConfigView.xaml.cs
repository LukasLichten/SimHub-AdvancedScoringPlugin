﻿using Advanced_Scoring.UI.Controller;
using Advanced_Scoring.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advanced_Scoring.UI.View
{
    /// <summary>
    /// Interaction logic for ManageDelta.xaml
    /// </summary>
    public partial class DeltaConfigView : UserControl
    {
        private DeltaConfigController Controller { get; set; }

        public DeltaConfigView()
        {
            InitializeComponent();
            
        }

        internal void SetController(DeltaConfigController controller)
        {
            Controller = controller;
            LvDeltas.ItemsSource = Controller.DataModel.Deltas;

            if (Controller.ImportMode)
                BtnPort.Content = "Import From File";

            GetSelectedIndex(); //To disable all the buttons
        }

        private void TbSearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void LvDeltas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetSelectedIndex();
        }

        private DeltaModel.DataRow GetSelectedIndex()
        {
            object item = LvDeltas.SelectedItem;

            if (item is DeltaModel.DataRow)
            {
                BtnCopy.IsEnabled = true;
                BtnDelete.IsEnabled = true;
                BtnPort.IsEnabled = true;
                return item as DeltaModel.DataRow;
            }
            else
            {
                BtnCopy.IsEnabled = false;
                BtnDelete.IsEnabled = false;
                BtnPort.IsEnabled = Controller != null ? Controller.ImportMode : false;
            }

            return null;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            var result = MessageBox.Show("Do you really want to delete the " + item.LapTime + " on "+ item.TrackName + " with " + item.CarName, "Are you sure?", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                Controller.DeleteItem(item);
            }
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            var item = GetSelectedIndex();

            if (item == null)
                return;

            Controller.StartCopyItem(item);
        }

        private void BtnPort_Click(object sender, RoutedEventArgs e)
        {
            if (Controller.ImportMode)
            {
                Controller.ImportItem();
                return;
            }


            var item = GetSelectedIndex();

            if (item == null)
                return;

            Controller.ExportItem(item);
        }
    }
}
