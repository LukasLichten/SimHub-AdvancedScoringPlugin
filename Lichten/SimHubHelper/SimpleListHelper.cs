﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lichten.SimHubHelper
{
    /// <summary>
    /// This is a simple implementation of a list helper
    /// If you have just a function that returns the current value and another one that returns the list, then you can use this
    /// </summary>
    public class SimpleListHelper : ListHelper
    {
        public Get Getter { set; get; }
        public List ListGetter { set; get; }

        public SimpleListHelper(Get getter, List listGetter)
        {
            Getter = getter;
            ListGetter = listGetter;
        }

        public override string GetCurrent()
        {
            return Getter.Invoke();
        }

        public override string[] GetList()
        {
            return ListGetter.Invoke().ToArray();
        }

        public delegate string Get();
        public delegate List<string> List();
    }
}
